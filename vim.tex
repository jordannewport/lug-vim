\documentclass{lug}

\newcommand{\T}{\texttt}
\newcommand{\caret}{\textasciicircum}

\usepackage{verbatim}

\title{Vim}
\author{Jordan Newport}

\begin{document}

\begin{frame}{History}
    \begin{itemize}
        \item \T{ed}
        \item \T{ex} by Bill Joy
        \item \T{vi}, also by Bill Joy
        \item \T{vim}: Vi iMproved by Bram Moolenaar
    \end{itemize}
\end{frame}

\begin{frame}{Survival Vim}
    \begin{itemize}
        \item \T{h}, \T{j}, \T{k}, \T{l} move the cursor left, down, up, right
        \item \T{i} to start typing in ``insert mode''
        \item \T{escape} to go out of typing back into ``normal mode''
        \item \T{:w} to save, \T{:q} to exit, \T{:wq} to save and exit
        \item If you get stuck somewhere weird, spam \T{escape}
        \item If you're feeling bold, make sure you're in ``normal mode'' and
            hit \T{u} to undo
    \end{itemize}
\end{frame}

\section{Why should you use vim?}

\begin{frame}{Vi(m) is on every machine}
    \begin{itemize}
        \item Vi or vim (depending on distro etc) is on \textit{every *nix
                machine there is}
        \item If you ever need a text editor on someone else's computer or some
            random server or if you broke everything else, it's there
    \end{itemize}
\end{frame}

\begin{frame}{Vi(m) is in every program}
    \begin{itemize}
        \item If you learn Vim keybindings, they are everywhere
        \item Browsers: Firefox (Tridactyl), Chromium (cVim), Qutebrowser, most
            terminal browsers
        \item Editors: Everything but nano. Even Emacs (evil mode)
        \item Window managers: i3, etc
        \item Libreoffice (Vibreoffice)
        \item Shells: zsh (\T{bindkey -v}), most others (\T{set -o vi})
    \end{itemize}
\end{frame}

\begin{frame}{But wait. There are more}
    \begin{itemize}
        \item Mail clients: mutt
        \item Terminal emulators: urxvt
        \item Pdf viewers: most. Zathura is especially good
        \item File managers: ranger
        \item Pagers: less
        \item Games: nethack, a lot of other open-source games
        \item Your mouse (see my i3 config)
    \end{itemize}
\end{frame}

\begin{frame}{Vim does not suck}
    \begin{itemize}
        \item Vim is lightweight
        \item Me using Neovim with a lot of extensions is less than 100M per
            instance, but JetBrains IDEs can hit swap
        \item Vim also does any language. You don't need:
            \begin{itemize}
                \footnotesize
                \item CLion
                \item IntelliJ
                \item Webstorm
                \item ...
            \end{itemize}
        \normalsize
        You only need vim
        \item Especially with Language Client: see
            https://microsoft.github.io/language-server-protocol/ and
            https://github.com/neoclide/coc.nvim
    \end{itemize}
\end{frame}

\begin{frame}{Powerful and fast}
    Value...
    \begin{itemize}
        \item muscle memory
        \item conciseness
        \item composability
        \item ability to specify arbitrary selections
        \item easy access to linewise tools
    \end{itemize}
    over intuition
\end{frame}

\section{Using Vim better}

\begin{frame}{Modes}
    Vim is a \textit{modal} editor. You are always in some mode
    \begin{itemize}
        \item Normal mode: the default. Mainly for navigation and special
            commands. Get here with \T{Escape}
        \item Ex mode: for doing weird things. Get here with \T{:}
            \begin{itemize}
                \item AKA command line mode
            \end{itemize}
        \item Insert mode: For writing things. See next slide
        \item Visual modes: for selecting text. Get here with \T{v} or \T{V} or
            \T{\caret v}
            \begin{itemize}
                \item \T{\caret} means ``Control +''
            \end{itemize}
    \end{itemize}
\end{frame}

\begin{frame}{Entering insert mode}
    \begin{itemize}
        \item \T{i} to insert on/before cursor
        \item \T{I} to insert at start of line
        \item \T{a} to insert after cursor
        \item \T{A} to insert at end of line
        \item \T{o} to make a new line below this one
        \item \T{O} to make a new line above this one
    \end{itemize}
\end{frame}

\begin{frame}{Fixing mistakes}
    \begin{itemize}
        \item \T{u} to undo your previous command
        \item \T{\caret R} to redo what you just undid
    \end{itemize}
\end{frame}

\begin{frame}{Navigating text}
    \begin{itemize}
        \item I already told you about \T{hjkl}
        \item \T{gg} to go to the top of the file
        \item \T{G} to go to the bottom of the file
        \item \textit{n}\T{gg} or \textit{n}\T{G} to go to line \textit{n}
        \item \T{w} to go forward a word
        \item \T{b} to go back a word
    \end{itemize}
\end{frame}

\begin{frame}{Navigating Text II}
    \begin{itemize}
        \item \T{f<char>} to go to the next instance of \T{<char>}
        \item \T{F<char>} to go to the previous instance of \T{<char>}
        \item \T{t<char>} to go to right before the next instance of \T{<char>}
        \item \T{T<char>} to go to right after the previous instance of \T{<char>}
        \item \T{;} to repeat previous \T{f}/\T{t} search
        \item \T{,} to repeat previous \T{f}/\T{t} search in opposite direction
    \end{itemize}
\end{frame}

\begin{frame}{Navigating Text III}
    \begin{itemize}
        \item \T{/} to search for a regex
        \item \T{n} to go to next search result
        \item \T{N} to go to previous search result
        \item \T{\caret} or \T{0} to go to start of line
        \item \T{\$} to go to end of line
    \end{itemize}
\end{frame}

\begin{frame}{Useful commands}
    \begin{itemize}
        \item \textit{n} to do a (text-modifying--not motion) command \textit{n}
            times
        \item \T{.} to repeat a previous command
    \end{itemize}
\end{frame}

\begin{frame}{Visual mode}
    \begin{itemize}
        \item \T{v} for visual mode--much like classic selections. Motion to
            extend selection, \T{esc} to cancel
        \item \T{V} for visual line mode--select whole lines
        \item \T{\caret v} for visual block mode--select in 2D
    \end{itemize}
\end{frame}

\begin{frame}{More commands}
    \begin{itemize}
        \item More complex vim commands work on the action-object model
            \begin{itemize}
                \item You put in an action and an object on which to operate
            \end{itemize}
        \item Objects:
            \begin{itemize}
                \item A motion (e.g. \T{2j} or \T{3w} or \T{fe})
                \item A text object (e.g. \T{iw} or \T{a\}})
                \item A visual selection
                \item Shorthand for just the current line is the command letter
                    repeated
            \end{itemize}
    \end{itemize}
\end{frame}

\begin{frame}{Copy, Paste, and Delete}
    \begin{itemize}
        \item \T{y} to yank (copy)
        \item \T{d} to delete (cut)
        \item \T{c} to change (also deletes/cuts)
        \item \T{x} to delete/cut one character
        \item \T{p} to paste after cursor
        \item \T{P} to paste before cursor
        \item if you are using gvim or neovim...prepend \T{"+} to use the
            system clipboard instead of a vim register
    \end{itemize}
\end{frame}

\begin{frame}{Other editing tasks}
    \begin{itemize}
        \item \T{= <object>} to autoformat those lines
        \item \T{:\%s/foo/bar/g} to search and replace \T{foo} with \T{bar}
            \begin{itemize}
                \item Replace \T{\%} with \T{startline, endline} to do this on
                    only some lines
                \item Replace \T{g} with \T{c} to get your confirmation for each
                    change
                \item \T{foo} can be any regex. Capture groups can be accessed
                    from the replacement
            \end{itemize}
    \end{itemize}
\end{frame}

\section{Essential configs and further resources}

\begin{frame}{But Escape is far away}
    You have six (!) options
    \begin{itemize}
        \item Suffer through it
        \item \T{\caret [} also types escape
        \item Remap a key combo (\T{jk} is popular) to escape
        \item (on all X desktop environments) replace caps lock with escape. See
            my dotfiles' \T{.Xmodmap} for one example
        \item (on fancy desktop environments) use your GUI tools to replace caps
            lock with escape
        \item use a keyboard layout that does this for you, e.g. \T{3l}
    \end{itemize}
\end{frame}

\begin{frame}[fragile]{Essential configs}
    These live in \T{\textasciitilde/.vimrc} or
    \T{\textasciitilde/.config/nvim/init.vim}
    \begin{itemize}
        \item Make your tabs not suck:
            \begin{verbatim}
set shiftwidth=4    " 4 space tab
set tabstop=4       " 4 space tab
set expandtab       " tabs replaced with spaces \end{verbatim}
        \item Give yourself line numbers and a backspace key (backspace thing
            only sometimes necessary):
            \begin{verbatim}
set number
set backspace=indent,eol,start \end{verbatim}
        \item Generally make vim behave behind the scenes:
            \begin{verbatim}
set nocompatible
set hidden \end{verbatim}
    \end{itemize}
\end{frame}

\begin{frame}[fragile]{Really convenient configs}
    \begin{itemize}
        \item Filetype specific line wrap and spellcheck:
            \footnotesize
            \begin{verbatim}
autocmd BufNewFile,BufRead *.md,*.txt setlocal tw=80
autocmd BufNewFile,BufRead *.md setlocal spell spelllang=en_us
            \end{verbatim}
            \normalsize
        \item Preserve undo across editing sessions:
            \begin{verbatim}
set undofile
            \end{verbatim}
    \end{itemize}
\end{frame}

\begin{frame}{Plugins}
    There are a ton of Vim plugins. Many of them suck. Here are some that don't:
    \begin{itemize}
        \item \T{vim-plug} is technically not a plugin but a plugin manager:
            https://github.com/junegunn/vim-plug
        \item Tim Pope (https://github.com/tpope) writes some good lightweight
            plugins, including:
            \begin{itemize}
                \item \T{vim-surround}
                \item \T{vim-commentary}
            \end{itemize}
        \item \T{coc.nvim} provides autocomplete (and such) for multiple
            lanugages: https://github.com/neoclide/coc.nvim
    \end{itemize}
\end{frame}

\begin{frame}{Vim Distributions}
    \begin{itemize}
        \item Neovim is a faster-moving fork of Vim
            \begin{itemize}
                \item It provides useful innovations sooner, so it has better
                    plugin support
                \item Also has clipboard support
            \end{itemize}
        \item Gvim is vim, but with clipboard support, a couple GUI tools, and
            an option to run outside of your terminal
        \item Vim is the standard
        \item Vi is the POSIX standard, but has significantly fewer features
    \end{itemize}
\end{frame}

\begin{frame}{More resources}
    \begin{itemize}
        \item My config: \footnotesize https://gitlab.com/jordannewport/dotfiles/blob/master/.config/nvim/init.vim
        \normalsize
        \item (actually useful) arcane lore: the accepted answer to \footnotesize
            https://stackoverflow.com/questions/1218390/what-is-your-most-productive-shortcut-with-vim
        \normalsize
        \item Type \T{:help <command you want help with>} from inside Vim to see
            the official documentation for that command
        \item Do you feel like you're spending too many keystrokes on something?
            Search it. There's probably a way to do it faster.
    \end{itemize}
\end{frame}

\end{document}
